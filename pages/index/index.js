//index.js
//获取应用实例
const app = getApp()


Page({
  data: {
    // motto: 'Hello World',
    // userInfo: {},
    // hasUserInfo: false,
    // canIUse: wx.canIUse('button.open-type.getUserInfo'),
    // temp:'30',
    // temprature:'28/34',
    // type:'晴',
    // city:'成都'
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    var that = this;
    var result = null;
    wx.request({
      url: 'http://apis.juhe.cn/simpleWeather/query?',
      data:{
        "city":"宜宾",
        "key":"399a7b8a71f13f6a8f779e52615cd91d"
      },
      success(res){
        result = res.data.result;
        var test = JSON.stringify(result);
        console.log(test);
        that.setData({
          city: result.city,
          temp: result.realtime.temperature,
          temprature: result.future[0].temperature,
          type: result.future[0].weather,
          arrtemp: result.future,
        })
      }
    })

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
